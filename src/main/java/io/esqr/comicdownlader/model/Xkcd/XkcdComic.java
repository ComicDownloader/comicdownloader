package io.esqr.comicdownlader.model.Xkcd;


import lombok.Getter;
import lombok.Setter;

/**
 * Created by konrad on 06.10.15.
 */
public class XkcdComic {
    //
    @Getter @Setter
    private int num;
    @Getter @Setter
    private String title;
    @Getter @Setter
    private String img;

    public XkcdComic(int id) {
        setNum(id);
    }


}
