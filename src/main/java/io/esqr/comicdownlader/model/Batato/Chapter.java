package io.esqr.comicdownlader.model.Batato;

import lombok.Getter;
import lombok.Setter;

public class Chapter {
    @Getter @Setter
    String name;
    @Getter @Setter
    String group;
    @Getter @Setter
    Comic comic;
    @Getter @Setter
    int id;
    @Getter @Setter
    String number;

    @Getter @Setter
    String hash;

}
