package io.esqr.comicdownlader.model.Batato;

import lombok.Getter;
import lombok.Setter;


public class Comic {

    @Getter @Setter
    String title;
    @Getter @Setter
    String author;
    @Getter @Setter
    float rating;
    @Getter @Setter
    int follows;
    @Getter @Setter
    int id;

}
