package io.esqr.comicdownlader;

import com.squareup.okhttp.*;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Response;
import io.esqr.comicdownlader.model.Batato.Chapter;

import javafx.concurrent.Task;
import okio.BufferedSink;
import okio.Okio;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.*;

import java.net.URL;
import java.util.concurrent.TimeUnit;

public class BatotoDownloader extends Task<Void> {
  //  String baseUrl;
    private volatile boolean running = true;
    private final static  OkHttpClient client=new OkHttpClient();
    Callback callback;
   // Callback htmlCallback;
    String chapterDir;
    Chapter chapter;
    int progress=0;
    public static final  HttpUrl baseUrl =HttpUrl.parse("http://bato.to");

     HttpUrl chapterUrl;
    final int[] pages = new int[1];

    public BatotoDownloader(Chapter chapter) {
        client.setConnectTimeout(5, TimeUnit.SECONDS);
        this.chapter = chapter;
        chapterUrl = baseUrl.newBuilder().addPathSegment("areader")
                .addQueryParameter("id", chapter.getHash())
                .build();


        chapterDir = System.getProperty("user.home") + "/Batoto/" + chapter.getComic().getTitle() + "/Chapter " + chapter.getNumber();

        File file = new File(chapterDir);
        if (!file.exists()) {
            if (!file.mkdirs()) {
                updateMessage("Błąd tworzenia katalogów");
            }
        }


    }

//        htmlCallback = new Callback() {
//            @Override
//            public void onFailure(Request request, IOException e) {
//                System.out.println(e.getMessage());
//                try {
//                    showRetryInfo();
//                } catch (InterruptedException e1) {
//                    e1.printStackTrace();
//                }
//
//            }
//
//            @Override
//            public void onResponse(Response response) throws IOException {
//                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
//
//                Document doc = Jsoup.parse(response.body().string(), baseUrl);
//
//             //   Element e = doc.getElementById("page_select");
//                Element x=doc.getElementById("ipbwrapper");
//                if(x!=null){
//                    Element reader=x.getElementById("reader");
//                    if(reader!=null){
//                        System.out.println(reader.html());
//
////                        System.out.println(reader.outerHtml());
////                        System.out.println(reader.toString());
//
//                        Element comic_wrap=reader.getElementById("comic_wrap");
//                        if(comic_wrap!=null) {
//                            pages[0] = comic_wrap.getElementById("moderation-bar").getElementById("page_select").children().size();
//
//                            updateMessage("Pobieranie");
//                            Element img = reader.getElementById("comic_page");
//
//                            if (img != null) {
////                System.out.println("img.src="+img.toString());
//                                System.out.println("img.src=" + img.text());
//
//
//                                String imageUrl = img.attr("src");
//
//                                URL chapterUrl = new URL(imageUrl);
//                                File file = new File(chapterDir + "/" + String.format("%03d", 1) + ".jpg");
//
//                                Request request = new Request.Builder()
//                                        .chapterUrl(chapterUrl).addHeader("Referer","http://bato.to/reader")
//                                        .build();
//
//                                Response response1 = client.newCall(request).execute();
//                                BufferedSink sink = Okio.buffer(Okio.sink(file));
//                                sink.writeAll(response1.body().source());
//                                sink.close();
//                                updateProgress((double) progress++ / pages[0], 1);
//                                System.out.println("imageURL: " + imageUrl);
//
//
//                                String[] tmp = imageUrl.split("/img");
//                                System.out.println("tmp: " + tmp);
//
//
//                                String address = "";
//
//                                for (int j = 0; j < tmp.length - 1; j++) {
//                                    address = address.concat(tmp[j]);
//                                    if (j < tmp.length - 2) {
//                                        address = address.concat("/img");
//                                    }
//                                }
//
//                                String myadress;
//                                for (int i = 2; i <= pages[0]; i++) {
//
//                                    myadress = address.concat("/img" + String.format("%06d", i) + ".jpg");
//                                    System.out.println(myadress);
//
//                                    Request request2 = new Request.Builder()
//                                            .chapterUrl(myadress)
//                                            .build();
//                                    Response response2 = client.newCall(request2).execute();
//                                    file = new File(chapterDir + "/" + String.format("%03d", i) + ".jpg");
//
//                                    sink = Okio.buffer(Okio.sink(file));
//                                    sink.writeAll(response2.body().source());
//                                    sink.close();
//
//                                }
//                                updateProgress(1, 1);
//                                updateMessage("Pobrano");
//                            } else {
//                                System.out.println("img==null ");
//
//                            }
//                        }else {
//                            System.out.println("comic_wrap==null ");
//
//                        }
//                    }
//                }
//            //    System.out.println("reader"+reader.toString());
//
//            }
//        };
//        callback=new Callback() {
//            @Override
//            public void onFailure(Request request, IOException e) {
//                System.out.println(e.getMessage());
//                try {
//                    showRetryInfo();
//                } catch (InterruptedException e1) {
//                    e1.printStackTrace();
//                }
//            }
//
//            @Override
//            public void onResponse(Response response) throws IOException {
//                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
//                updateProgress((double) progress++ / pages[0], 1);
//
//            }
//        };
//    }


    Response pageRequest(int page) throws IOException {
    final HttpUrl pageUrl = chapterUrl.newBuilder()
            .addQueryParameter("p", String.valueOf(page))
            .build();
    System.out.println("Executed pageUrl:" + chapterUrl);

    Request request = new Request.Builder()
            .url(pageUrl).addHeader("Referer", "http://bato.to/reader")
            .build();
        return client.newCall(request).execute();

}
    URL parsePageWithPages(Response response) throws IOException {
        if (!response.isSuccessful()) {
            System.out.println("!response.isSuccessful()");

            throw new IOException("Unexpected code " + response);
        } else {
            Document doc = Jsoup.parse(response.body().string());
            Element e = doc.getElementById("page_select");
            if (e != null) {
                pages[0] = e.children().size();
                e = doc.getElementById("comic_wrap").getElementById("comic_page");
                return new URL(e.attr("src"));
            } else {
                System.out.println("page_select==NULL");

            }
        }
        return null;
    }
    URL parsePage(Response response) throws IOException {
        if (!response.isSuccessful()) {
            System.out.println("!response.isSuccessful()");

            throw new IOException("Unexpected code " + response);
        } else {
            Document doc = Jsoup.parse(response.body().string());
            Element e = doc.getElementById("page_select");
            if (e != null) {
                pages[0] = e.children().size();
                e = doc.getElementById("comic_wrap").getElementById("comic_page");
                return new URL(e.attr("src"));
            } else {
                System.out.println("page_select==NULL");

            }
        }
        return null;
    }
    void downloadImage(int page,URL imageUrl) throws IOException {
        File file = new File(chapterDir + "/" + String.format("%03d", page) + ".jpg");
        Request requestImage = new Request.Builder()
                .url(imageUrl)
                .build();


        Response response1 = client.newCall(requestImage).execute();
        BufferedSink sink = Okio.buffer(Okio.sink(file));
        sink.writeAll(response1.body().source());
        sink.close();
        updateProgress((double) progress++ / pages[0], 1);
    }

    @Override
    protected Void call() throws Exception {
        Response response= pageRequest(1);
        URL imageUrl= parsePageWithPages(response);
        downloadImage(1,imageUrl);
        int p;
        for(p = 2;p<=pages[0];p++){
            response= pageRequest(p);
            imageUrl= parsePage(response);
            downloadImage(p,imageUrl);
        }

        updateProgress(1, 1);
        updateMessage("Pobrano");


        return null;
    }





        public void pauseThread() throws InterruptedException {
            running = false;
        updateMessage("Pauzowanie");
        }

        public void resumeThread() {
            running = true;
        updateMessage("Pobieranie");
        }

        public void toggleThread() {
            if (running) {
                try {
                    pauseThread();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                resumeThread();
            }
        }

        void showRetryInfo() throws InterruptedException {
            for (int j = 5; j > 0; j--) {
            updateMessage("Błąd; ponawianie za " + j);
                if (!running) {
                updateMessage("Pauza");
                    while (!running)
                        Thread.sleep(100);
                }
                Thread.sleep(1000);
            }
        }
    public Chapter getChapter() {

        return chapter;
    }



    }
