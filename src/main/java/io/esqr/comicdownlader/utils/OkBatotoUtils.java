package io.esqr.comicdownlader.utils;

import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import io.esqr.comicdownlader.SearchResult;
import io.esqr.comicdownlader.model.Batato.Chapter;
import io.esqr.comicdownlader.model.Batato.Comic;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OkBatotoUtils {
    private final static OkHttpClient client = new OkHttpClient();
    public static final  HttpUrl baseUrl =HttpUrl.parse("http://bato.to");
    public OkBatotoUtils() {
        client.setConnectTimeout(5, TimeUnit.SECONDS);
        System.out.println(baseUrl.toString());

    }


    public static SearchResult searchComics(String title, int page) throws IOException {

        List<Comic> results = new ArrayList<>();
          final HttpUrl url  = baseUrl.newBuilder().addPathSegment("search")
                                            .addQueryParameter("name", title)
                                            .addQueryParameter("p", String.valueOf(page))
                                            .build();
        System.out.println(url.toString());

        Request request = new Request.Builder()
                .url(url)
                .build();
        final  Response response= client.newCall(request).execute();
        Document doc=Jsoup.parse(response.body().string(), url.toString());

        Elements rawcomics = doc.select(".chapters_list tr:not([id^=comic_], .header, #show_more_row)");
        for (Element rawcomic : rawcomics) {
            Comic comic = new Comic();
            Elements data = rawcomic.select("td");
            comic.setTitle(data.get(0).select("strong").text());
            String[] tmp = data.get(0).select("strong a").attr("href").split("-r");
            comic.setId(Integer.parseInt(tmp[tmp.length - 1]));
            comic.setAuthor(data.get(1).text());
            String rawrating = data.get(2).select("div").attr("title");
            comic.setRating(Float.parseFloat(rawrating.substring(0, rawrating.length() - 2)));
            comic.setFollows(Integer.parseInt(data.get(4).text().replace('-', '0')));
            results.add(comic);
        }
        boolean moreResults = doc.getElementById("show_more_row") != null;
        return new SearchResult(results, moreResults);
    }

    public static List<Chapter> getChapters(Comic comic) throws IOException {
        System.out.println("start");
        List<Chapter> results = new ArrayList<>();


          HttpUrl url =  baseUrl.newBuilder()
                                .addPathSegment("comic")
                                .addPathSegment("_")
                                .addPathSegment("comics")
                                .addPathSegment(comic.getTitle() + "-r"  + comic.getId())
                  .build();

          Request request = new Request.Builder().url(url)
                                                 .build();
//        System.out.println("Connect:"+url);

        Response response= client.newCall(request).execute();

        Document doc=Jsoup.parse(response.body().string(), url.toString());
        System.out.println("end");
//        System.out.println(doc.toString());

        Elements rawchapters = doc.select(".lang_English.chapter_row");
//        rawchapters.forEach(chapter -> System.out.println(chapter.toString()));

        for (Element rawchaper : doc.select(".lang_English.chapter_row") ){
            Chapter chapter = new Chapter();
            Elements data = rawchaper.select("td");
            chapter.setName(data.get(0).text());
//            String ahref = data.get(0).select("a").get(0).attr("href");
//            String[] tab=ahref.split("/");
//        for(String s :tab){
//                    System.out.println(s);
//            System.out.println("Tab length"+tab.length);
//
//        }

//            int a= Integer.parseInt(data.get(0).select("a").get(0).attr("href").split("/")[5]);
       //     chapter.setId(Integer.parseInt(data.get(0).select("a").get(0).attr("href").split("/")[5]));
            String hash=data.get(0)
                            .select("a").get(0).attr("href")
                            .split("/")[3]
                            .substring(7);
            chapter.setHash(hash);
            System.out.println(chapter.getHash());
            chapter.setComic(comic);
            chapter.setGroup(data.get(2).text());
            Pattern r = Pattern.compile("^(?:Vol\\.\\d+ )?Ch\\.([\\d\\.]+)");
            Matcher m = r.matcher(chapter.getName());
            m.find();
            chapter.setNumber(m.group(1));
//            System.out.println("rawChapter:"+chapter.getName());

            results.add(chapter);
        }
//        System.out.println("getChapters:");
//        results.forEach(chapter -> System.out.println(chapter.getName()));

        return results;
    }
}
