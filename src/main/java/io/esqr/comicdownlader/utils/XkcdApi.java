package io.esqr.comicdownlader.utils;

import retrofit.*;


/**
 * Created by konrad on 06.10.15.
 */
public class XkcdApi {
    private static final String BASE_URL = "http://www.xkcd.com/";
    private XkcdService apiService;
    private XkcdLastService lastService;

    public XkcdApi()
    {



         apiService = new Retrofit.Builder()
                 .baseUrl(BASE_URL)
                 .addConverterFactory(GsonConverterFactory.create())
                 .build()
                 .create(XkcdService.class);


        lastService = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(XkcdLastService.class);



    }

    public XkcdService getApiService() {
        return apiService;
    }
    public XkcdLastService getLastService() {
        return lastService;
    }

}
