package io.esqr.comicdownlader.utils;//package io.esqr.comicdownlader.utils;


import io.esqr.comicdownlader.model.Xkcd.XkcdComic;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Path;

//614/info.0.json
    public interface XkcdService {
        @GET("/{id}/info.0.json")
        Call<XkcdComic> comicRequest(
                @Path("id") int id
               // ,Callback<String> cb
        );

    }


