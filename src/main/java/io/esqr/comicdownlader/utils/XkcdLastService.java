package io.esqr.comicdownlader.utils;//package io.esqr.comicdownlader.utils;

import io.esqr.comicdownlader.model.Xkcd.XkcdComic;
import retrofit.Call;
import retrofit.http.GET;

public interface XkcdLastService {
    @GET("/info.0.json")
        //  Call
    Call<XkcdComic> comicRequest(
            // ,Callback<String> cb
    );

}
