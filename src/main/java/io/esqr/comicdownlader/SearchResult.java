package io.esqr.comicdownlader;

import io.esqr.comicdownlader.model.Batato.Comic;

import java.util.List;

public class SearchResult {
    private final List<Comic> results;
    private final boolean moreResults;

    public SearchResult(List<Comic> results, boolean moreResults) {
        this.results = results;
        this.moreResults = moreResults;
    }

    public boolean isMoreResults() {
        return moreResults;
    }

    public List<Comic> getResults() {
        return results;
    }
}
