package io.esqr.comicdownlader;

import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.ProgressBarTableCell;
import javafx.util.Callback;

import java.lang.reflect.Field;

public class DownloadProgress<S> extends ProgressBarTableCell<S> {

    public static <S> Callback<TableColumn<S, Double>, TableCell<S, Double>> lsdMode() {
        return param -> new DownloadProgress<>();
    }

    private ProgressBar progressBar;

    public DownloadProgress() {
        Field field;
        try {
            field = ProgressBarTableCell.class.getDeclaredField("progressBar");
            field.setAccessible(true);
            this.progressBar = (ProgressBar) field.get(this);
            this.progressBar.progressProperty().addListener((observable1, oldValue, newValue) -> {
                double progress = newValue == null ? 0 : newValue.doubleValue();
                if (progress == 1) {
                    this.progressBar.setStyle("-fx-accent: green;");
                }
            });
        } catch (IllegalAccessException | NoSuchFieldException e) {
            e.printStackTrace();
        }

    }
}
