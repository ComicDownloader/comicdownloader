package io.esqr.comicdownlader.controler;

import io.esqr.comicdownlader.BatotoDownloader;
import io.esqr.comicdownlader.DownloadProgress;
import io.esqr.comicdownlader.DownloadXkcdTask;
import io.esqr.comicdownlader.SearchResult;
import io.esqr.comicdownlader.model.Batato.Chapter;
import io.esqr.comicdownlader.model.Batato.Comic;
import io.esqr.comicdownlader.model.Xkcd.XkcdComic;
import io.esqr.comicdownlader.utils.OkBatotoUtils;
import io.esqr.comicdownlader.utils.XkcdApi;
import io.esqr.comicdownlader.utils.XkcdService;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import retrofit.Call;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

public class MainWindowController implements Initializable {


    @FXML
    private TableColumn<Comic, String> titleCol;

    @FXML
    private TableView<Chapter> chaptersTable;

    @FXML
    private Button downloadButton;

    @FXML
    private Button searchButton;

    @FXML
    private TextField searchField;

    @FXML
    private TableColumn<Comic, Integer> followsCol;

    @FXML
    private TableColumn<Chapter, String> chapterCol;

    @FXML
    private TableColumn<Comic, String> authorCol;

    @FXML
    private TableColumn<BatotoDownloader, String> dlMangaCol;

    @FXML
    private TableColumn<Comic, Float> ratingCol;

    @FXML
    private Button moreresultsButton;

    @FXML
    private TitledPane searchResults;

    @FXML
    private TableView<Comic> comicsTable;

    @FXML
    private TitledPane chaptersPane;

    @FXML
    private TableColumn<Chapter, String> groupCol;



    @FXML
    private TableColumn<BatotoDownloader, Double> dlProgressCol;

    @FXML
    private Label statusText;

    @FXML
    private TableColumn<BatotoDownloader, String> dlChapterCol;

    @FXML
    private TableColumn<BatotoDownloader, String> dlGroupCol;

    @FXML
    private TableColumn<BatotoDownloader, String> dlStateCol;


    @FXML
    private TableView<BatotoDownloader> downloadsTable;

    @FXML
    private Button pauseButton;

    @FXML
    public TableView<XkcdComic> xkcdComicTableView;

    @FXML
    public Button xkcdDownloadButton;

    @FXML
    private TableColumn<XkcdComic, String> xkcdTitleCol;
    @FXML
    private TableColumn<XkcdComic, String> xkcdImgCol;
    @FXML
    private TableColumn<XkcdComic, String> xkcdIdCol;
    XkcdComic xkcdComic;
    ObservableList<XkcdComic> xkcdComicsTableData=    FXCollections.observableArrayList();

    @FXML
    public TableView<XkcdComic> xkcdComicsTable;

    int searchPage = 1;
    String moreResultsPhrase;
    ExecutorService executor;


    @Override
    public void initialize(URL url, ResourceBundle rb) {
//        executor = Executors.newFixedThreadPool(6);

        executor = Executors.newFixedThreadPool(6, new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                Thread t = Executors.defaultThreadFactory().newThread(r);
                t.setDaemon(true);
                return t;
            }
        });


//        setXkcdTab();
        setSearchTab();
        setDownloadTab();

    }

    void setStatus(String status) {
        statusText.setText(status);
    }

    void newSearch() {
        String searchPhrase = searchField.getText();
        moreResultsPhrase = searchPhrase;
        searchPage = 1;
        search(searchPhrase);
    }

    void moreResults() {
        search(moreResultsPhrase);
        searchPage++;
    }

    void search(String searchPhrase) {
        Thread searchThread = new Thread(() -> {
            try {
                setSearchDisable(true);
                SearchResult searchResult = OkBatotoUtils.searchComics(searchPhrase, searchPage);
                ObservableList<Comic> data = FXCollections.observableArrayList(searchResult.getResults());
                if (searchPage > 1)
                    comicsTable.getItems().addAll(data);
                else
                    comicsTable.setItems(data);
                searchResults.setExpanded(true);
                if (searchResult.isMoreResults())
                    moreresultsButton.setDisable(false);
                else {
                    moreresultsButton.setDisable(true);
                }
            } catch (Exception e) {
                Platform.runLater(() -> setStatus(e.toString()));
                e.printStackTrace();
            } finally {
                setSearchDisable(false);
            }
        });
        searchThread.start();
    }

    void setSearchDisable(boolean value) {
        searchField.setDisable(value);
        searchButton.setDisable(value);
        searchResults.setDisable(value);
        chaptersPane.setDisable(value);
    }

    void setSearchTab() {
        titleCol.setCellValueFactory(new PropertyValueFactory<>("title"));
        authorCol.setCellValueFactory(new PropertyValueFactory<>("author"));
        ratingCol.setCellValueFactory(new PropertyValueFactory<>("rating"));
        followsCol.setCellValueFactory(new PropertyValueFactory<>("follows"));
        chapterCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        groupCol.setCellValueFactory(new PropertyValueFactory<>("group"));

        chaptersTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        searchButton.setOnMouseClicked(event -> newSearch());
        searchField.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER)
                newSearch();
        });
        moreresultsButton.setOnMouseClicked(event -> moreResults());
        comicsTable.setOnMouseClicked(event -> {
            if (event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 2) {
                showChapters(comicsTable.getSelectionModel().getSelectedItem());
            }
        });

    }

    void showChapters(Comic comic) {
        Thread t = new Thread(() -> {
            try {
                setSearchDisable(true);
                ObservableList<Chapter> data = FXCollections.observableArrayList(OkBatotoUtils.getChapters(comic));
//                System.out.println("showChapters:");
//                data.forEach(chapter -> System.out.println(chapter.getName()));

                chaptersTable.setItems(data);
                chaptersPane.setExpanded(true);
            } catch (Exception e) {
                Platform.runLater(() -> setStatus(e.toString()));
                e.printStackTrace();
            } finally {
                setSearchDisable(false);
            }
        });
        t.start();
    }

    void setDownloadTab() {

        dlChapterCol.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getChapter().getName()));
        dlGroupCol.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getChapter().getGroup()));
        dlMangaCol.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getChapter().getComic().getTitle()));
        dlStateCol.setCellValueFactory(new PropertyValueFactory<>("message"));
        dlProgressCol.setCellValueFactory(new PropertyValueFactory<>("progress"));

        dlProgressCol.setCellFactory(DownloadProgress.<BatotoDownloader>lsdMode());

        downloadsTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        pauseButton.setOnMouseClicked(event -> {
            ObservableList<BatotoDownloader> tasks = downloadsTable.getSelectionModel().getSelectedItems();
            for (BatotoDownloader BatatoTask : tasks) {
                BatatoTask.toggleThread();
            }
        });

        downloadButton.setOnMouseClicked(event -> {
            ObservableList<Chapter> chapters = chaptersTable.getSelectionModel().getSelectedItems();
            for (Chapter chapter :chapters) {
                BatotoDownloader batotoDownloader=new BatotoDownloader(chapter);
                downloadsTable.getItems().add(batotoDownloader);
                System.out.println("After execute : "+ chapter.getHash());
                executor.execute(batotoDownloader);
            }
        });

    }

    void setXkcdTab() {
        XkcdApi xkcdApi = new XkcdApi();
        XkcdService service = xkcdApi.getApiService();
//        io.esqr.comicdownlader.utils.XkcdLastService lastService=xkcdApi.getLastService();
//        comicsTable.setDisable(true);
        for (int i = 1; i < 6; i++) {
            Runnable worker = new WorkerThread(i, service);
            executor.execute(worker);//calling execute method of ExecutorService
        }
        xkcdTitleCol.setCellValueFactory(
                new PropertyValueFactory<>("title")
        );
        xkcdImgCol.setCellValueFactory(
                new PropertyValueFactory<>("img")
        );
        xkcdIdCol.setCellValueFactory(
                new PropertyValueFactory<>("num")
        );
        xkcdComicsTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        xkcdComicsTable.setItems(xkcdComicsTableData);


        xkcdDownloadButton.setOnMouseClicked(event -> {
            ObservableList<XkcdComic> selectionChapters = xkcdComicsTable.getSelectionModel().getSelectedItems();
            for (int i = selectionChapters.size() - 1; i >= 0; i--) {
                DownloadXkcdTask task2 = new DownloadXkcdTask(selectionChapters.get(i));
//                downloadsTable.getItems().add(task2);
                executor.execute(task2);
            }

        });

    }

    class WorkerThread extends Task<Void> {
        private int id;
        XkcdService service;

        public WorkerThread(int id,XkcdService service){
            this.id=id;
            this.service=service;


        }
        @Override
        public Void call() {
            Call<XkcdComic> call;

            try {
                for (int i = 100*id-99; i < 100*id+1; i++) {

                    call = service.comicRequest(i);

                    xkcdComic = call.execute().body();
                    synchronized (xkcdComicsTableData){
                        xkcdComicsTableData.add(xkcdComic);}                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }


}
