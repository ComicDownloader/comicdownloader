package io.esqr.comicdownlader.controler;

import io.esqr.comicdownlader.DownloadXkcdTask;

import io.esqr.comicdownlader.model.Xkcd.XkcdComic;
import io.esqr.comicdownlader.utils.XkcdApi;
import io.esqr.comicdownlader.utils.XkcdService;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import retrofit.Call;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainWindowController2 implements Initializable {

    @FXML
    public TableView<XkcdComic> comicsTable;
//    @FXML
//    private TableView<DownloadXkcdTask> downloadsTable;
    @FXML
    public Button downloadButton;

    @FXML
    private TableColumn<XkcdComic, String> titleCol;
    @FXML
    private TableColumn<XkcdComic, String> imgCol;
    @FXML
    private TableColumn<XkcdComic, String> idCol;
    XkcdComic comic;
    ObservableList<XkcdComic> comicsTableData=FXCollections.observableArrayList();

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        ExecutorService executor = Executors.newFixedThreadPool(6);
//        ExecutorService executor = Executors.newFixedThreadPool(9, new ThreadFactory() {
//            @Override
//            public Thread newThread(Runnable r) {
//                Thread t = Executors.defaultThreadFactory().newThread(r);
//                t.setDaemon(true);
//                return t;
//            }
//         });

        XkcdApi xkcdApi = new XkcdApi();
        XkcdService service = xkcdApi.getApiService();
        io.esqr.comicdownlader.utils.XkcdLastService lastService=xkcdApi.getLastService();
//        comicsTable.setDisable(true);
        for (int i = 1; i < 6; i++) {
            Runnable worker = new WorkerThread(i,service);
            executor.execute(worker);//calling execute method of ExecutorService
        }
//        Thread xkcdLastThread = new Thread(() -> {
//
//            comicsTableData = FXCollections.observableArrayList();
//
//            Call<XkcdComic> call2 = lastService.comicRequest();
//
//            try {
//                comic = call2.execute().body();
//                comicsTableData.add(comic);
//
//
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//
//        });




        titleCol.setCellValueFactory(
                new PropertyValueFactory<>("title")
        );
        imgCol.setCellValueFactory(
                new PropertyValueFactory<>("img")
        );
        idCol.setCellValueFactory(
                new PropertyValueFactory<>("num")
        );
        comicsTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        comicsTable.setItems(comicsTableData);
                Thread blockGui = new Thread(() -> {
                            while (true) {
            if (executor.isTerminated()) {
//                synchronized (comicsTable){
//                comicsTable.setDisable(false);}
                executor.shutdown();

                break;
            }
        }


        });
        blockGui.start();


        downloadButton.setOnMouseClicked(event -> {
            ObservableList<XkcdComic> selectionChapters = comicsTable.getSelectionModel().getSelectedItems();
            for (int i = selectionChapters.size() - 1; i >= 0; i--) {
                DownloadXkcdTask task = new DownloadXkcdTask(selectionChapters.get(i));
//                downloadsTable.getItems().add(task);
                executor.execute(task);
            }

        });


    }

    class WorkerThread implements Runnable {
        private int id;
        XkcdService service;

        public WorkerThread(int id,XkcdService service){
            this.id=id;
            this.service=service;


        }
        @Override
        public void run() {
            Call<XkcdComic> call;

            try {
                for (int i = 100*id-99; i < 100*id+1; i++) {

                    call = service.comicRequest(i);

                    comic = call.execute().body();
                    synchronized (comicsTableData){
                        comicsTableData.add(comic);}                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    }
