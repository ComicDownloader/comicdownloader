package io.esqr.comicdownlader;

import io.esqr.comicdownlader.controler.MainWindowController;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

public class Main extends Application {

    public static void main(String[] args) {
        System.setProperty("prism.lcdtext", "false");
        launch(args);
    }
//final Stage stage=
    @Override
    public void start(Stage primaryStage) throws  Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("io/esqr/comicdownlader/MainWindow.fxml"));
        MainWindowController controller = new MainWindowController();
        loader.setController(controller);
        Parent root = loader.load();
        primaryStage.setTitle("ComicDownloader");
        primaryStage.setScene(new Scene(root, 600, 450));
        //500x275
        primaryStage.show();
    }
}
