package io.esqr.comicdownlader;

import io.esqr.comicdownlader.model.Xkcd.XkcdComic;
import javafx.concurrent.Task;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

public class DownloadXkcdTask extends Task<Void> {
    XkcdComic chapter;
    String dir;
    final String baseUrl;
    private volatile boolean running = true;
    String title;
    public DownloadXkcdTask(XkcdComic chapter) {
//                                System.out.println(chapter.getTitle());

        baseUrl = chapter.getImg();
         title = chapter.getTitle();
        dir = System.getProperty("user.home") + "/Batoto/" + "/Xkcd/" ;

        this.chapter=chapter;
        updateMessage("Oczekiwanie");

    }

    @Override
    protected Void call() throws Exception {

        try {
            File file = new File(dir);
            if (!file.exists()) {
                if (!file.mkdirs()) {
                    updateMessage("Błąd tworzenia katalogów");
                    return null;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if(!downloadFile(baseUrl)) {
         //   showRetryInfo();
        }
//        else {
//            updateProgress((double) i / pages, 1);
//        }
        return null;
    }







    boolean downloadFile(String url) {
        try {URL url2=new URL(url);
            System.out.println(chapter.getTitle());
            URLConnection connection = url2.openConnection();
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);
            ReadableByteChannel rbc = Channels.newChannel(connection.getInputStream());
            String[] tmp = url.split("\\.");
            FileOutputStream fos = new FileOutputStream(dir  +title+"." +tmp[tmp.length - 1]);
            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


}
